Yilin's GDBinit Dotfile
================================================================================
Holds my preferred GDBinit settings.

Largely taken from [here.](https://github.com/gdbinit/Gdbinit/)
