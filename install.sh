#!/bin/bash

# EFFECTS:  Adds a home folder symlink to the gdbinit in this folder.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -e ~/.gdbinit ]; then
    mv ~/.gdbinit $DIR/gdbinit_OLD
fi

if [ -e ~/.gdbinit.local ]; then
    mv ~/.gdbinit.local $DIR/gdbinit_local_OLD
fi

ln -s $DIR/gdbinit ~/.gdbinit
ln -s $DIR/gdbinit_local ~/.gdbinit.local
